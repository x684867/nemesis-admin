#!/usr/bin/env python 
#
# setup.py
# Setup Configuration for Nemesis Admin
# (c) 2015 Sam Caldwell.  MIT License
#
#
import __init__
import nemesis
import sys,os,inspect
from setuptools import setup, find_packages  
from codecs import open  
from os import path

cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(inspect.getfile( inspect.currentframe() ))[0]
    )
)
here = cmd_folder
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(cmd_folder,"module")
    )
)
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

# Get the long description from the relevant file
with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nemesis-admin',
    version='0.0.1',
    description='Nemesis Admin Tool (cli)',
    long_description=long_description,
    url='https://bitbucket.org/x684867/nemesis-admin',
    author='Sam Caldwell',
    author_email='mail@samcaldwell.net',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='nemesis key management encryption security',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'fabric',
    ],
    extras_require = {
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    include_package_data = True,
    package_data={
        'package_data': ['package_data.dat'],
    },
    data_files=[('pkg_config_data', ['module/data/config.yaml'])],
    entry_points={
        'console_scripts': [
            'nemesis=nemesis:main',
            'nemesis-init=nemesis:initialize'
        ],
    },
)
