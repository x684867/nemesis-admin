#!/usr/bin/env python
#
# ___init___.py
# (c) 2015 Sam Caldwell.  MIT License
#
#
import os
import sys
import inspect

cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(inspect.getfile( inspect.currentframe() ))[0]
    )
)
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(cmd_folder,"module")
    )
)
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

