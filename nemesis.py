#!/usr/bin/env python
#
# (c) 2014 Sam Caldwell.  MIT License.
#
# This is the main file for the Nemesis
# Administrator CLI Tool.
#
import __init__
import os
import sys
import inspect
import logging
from cliParser import cliParser
from cliExecutor import cliExecutor

class Nemesis:
    def __init__(
        self,
        commandList=[
            'help',
            'create',
            'upload-file',
            'link',
            'unlink'
        ],
        debug_mode=False
    ):
        #
        # To do...  configure logging facility
        #
        try:
            parser=cliParser(commandList)
            self.tokens=parser.tokens
        except Exception as e:
            logging.error("Nemesis constructor(parser):{}".format(e))
            sys.exit(1)
        try:
            self.__executor=cliExecutor(commandList)
        except Exception as e:
            logging.error("Nemesis constructor(executor):{}".format(e))
            sys.exit(2)

    @property 
    def tokens(self):
        return self.__tokens
    
    @tokens.setter
    def tokens(self,value):
        if isinstance(value,validator):
            self.__tokens=value
        else:
            print "cliParser.tokens setter expects Validator() child class."
            sys.exit(1)

    def initialize(self):
        logging.info("Nemesis::initialize() not implemented.")
        pass

    def main(self):
        try:
            self.__executor.execute(self.tokens)
        except Exception as e:
            logging.error("Error executing({}),{}".format(self.tokens,e))
            sys.exit(3)
 

def main():
    n=Nemesis()
    n.main()

def initialize():
    n=Nemesis()
    n.initialize()

if __name__ == "__main__":
    main()