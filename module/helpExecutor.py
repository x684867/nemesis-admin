#!/usr/bin/env python
#
# helpExecutor.py 
# CLI help utility.
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-25
#
import os
import sys
import inspect
import logging
from executorBase import Executor

class HelpExecutor (Executor):
    def __init__(self,token):
        if token.command!="help":
            raise Exception ("Bad routing.  Expected help command.  Encountered:{}".format(self.tokens.command)
            )
        if type(token.context) != str:
            raise Exception("Error: context not set by parser")

    def basedir(self,subdir=''):
        return os.path.join(
            os.path.realpath(
                os.path.abspath(
                    os.path.split(
                        inspect.getfile( inspect.currentframe() )
                    )[0]
                )
            ),
            subdir
        )

    def execute(self,token):
        try:
            token.context=token.context.lower()
            swidth=int(os.popen('stty size', 'r').read().split()[1])
            fp=""
            try:
                fp=self.basedir('data/help/'+token.context+'.help')
            except Exception as e:
                sys.exit(3)
            try:
                with open(fp,'r') as f:
                    print "="*swidth
                    print """Nemesis Command-Line Administration Tool (cli)\n\t(c) 2015 Sam Caldwell. MIT License.\n\tmail@samcaldwell.net"""
                    print "="*swidth
                    if token.context=="generic":
                        print "subject:<none>"
                    else:
                        print "subject:{}".format(token.context)
                    print "-"*swidth
                    print(f.read())
                    print """\nSuggestions?  Contact mail@samcaldwell.net."""
                    print "="*swidth
            except IOError as e:
                try:
                    if token.context == "generic":
                        print "FATAL ERROR LOADING HELP FILES."
                        sys.exit(1)
                    else:
                        token.context="generic"
                        self.execute(token)
                except Exception as e:
                    logging.error("HelpExecutor cannot read .help files.  Error:{}".format(e))
                    sys.exit(2)
        except Exception as e:
            logging.error("HelpExecutor::execute():{}".format(e))
            sys.exit(1)

    @property
    def context(self):
        return self.tokens.__context

    @context.setter
    def context(self,value):
        if type(value) is str:
            self.tokens.__context=value
        else:
            raise Exception("Error: help context can only be a string.")
