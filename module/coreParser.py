#!/usr/bin/env python
#
# coreParser.py 
# Core Parser Class for Nemesis Admin CLI
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-2
#
# parse the general command-line pattern, as follows:
#
#   nemesis <command> <arg1> <arg2> <arg3> <arg4> <arg5>
#
# and return a simple <dict> as follows:
#
#   result = {
#               command: <str>,
#               argArray: [ <str>,<str>,<str>,...,<str> ]
#   }
#
import sys
import argparse
from parsedTokens import ParsedTokens

class CoreParser:
    __tokens=ParsedTokens()

    def __init__(self):
        parser=argparse.ArgumentParser(
            description="""Nemesis CLI Admin Tool""",
            epilog="""Documentation:https://samcaldwell.atlassian.net/wiki/pages/viewpage.action?pageId=917887"""
        )
        parser.add_argument(
            'command',
            metavar='command',
            type=str,
            help='Nemesis CLI command'
        )
        parser.add_argument(
            'args',
            metavar='arg_list',
            type=str,
            nargs='+',
            help='Argument List for specific command.  See docs.'
        )
        raw=parser.parse_args()
        self.__tokens.command=raw.command
        self.__tokens.args=raw.args

    @property 
    def tokens(self):
        return self.__tokens
        











if __name__=="__main__":
    print """
                This file defines the coreParser class for Nemesis 
                Command-Line Administrator Tool.

                Executing this script directly will only execute unit 
                tests.  It is recommended that you execute this file
                with the following syntax for testing:

                python coreParser.py command arg1 arg2 arg3 arg4 arg5
    
                Testing...
    """
    p=CoreParser()
    print "coreParser Results:{}".format(p.tokens.dump())

