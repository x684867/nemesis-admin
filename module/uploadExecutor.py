#!/usr/bin/env python
#
# uploadExecutor.py 
# CLI file upload utility.
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-5
#
import os
import sys
import logging
from tokenClasses import HashString, FQDN
from executorBase import Executor

class UploadExecutor (Executor):
    def __init__(self,token):
        try:
            if token.command != "upload-file":
                raise Exception ("Bad routing.  Expected upload command.  Encountered:{}".format(self.tokens.command)
                )
            if type(token.fileName) is not str:
                raise Exception("Error: parser has not defined fileName")
            if type(token.mode) is str:
                if token.mode not in ['singleObject','keyValueObject']:
                    raise Exception("Error, invalid upload mode")
            else:
                raise Exception("Error: parser has not defined mode")

            if type(token.delimiter) is str:
                if len(token.delimiter) < 1:
                    raise Exception("Error:invalid delimiter char")
            else:
                raise Exception("Error: parser has not defined delimiter")
        except Exception as e:
            print "UploadExecutor Error:{}".format(e)
    """
    """
    def execute(self,token):
        objectId=self.upload(
            token.command, 
            token.brokerHost,
            token.fileName,
        )
        if token.mode == 'singleObject':
            try:
                self.runRemote(
                    "upload-file",
                    "{} {} {}".format(
                        token.mode,
                        token.delimiter
                    )
                )
            except Exception as e:
                logging.error("UploadExecutor::execute():{}".format(e))
                sys.exit(1)
        elif token.mode:
            try:
                self.runRemote(
                    "upload-file",
                    "{} {} {}".format(
                        token.mode,
                        token.delimiter
                    )
                )
            except Exception as e:
                logging.error("UploadExecutor::execute():{}".format(e))
                sys.exit(1)
        else:
            raise Exception("UploadExecutor::execute():{}".format(e))
