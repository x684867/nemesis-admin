#!/usr/bin/env python
#
# registerParser.py 
# Register Command Parser Class for Nemesis Admin CLI
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-4
# Doc: https://samcaldwell.atlassian.net/wiki/pages/viewpage.action?pageId=917876

import sys
from tokenClasses import HashString,FQDN
from validatorBase import Validator

class RegisterParser(Validator):

    def __init__(self,tokens):
        self.tokens=tokens
        if self.tokens.command!="register":
            raise Exception ("Bad routing. Expected register command.  Encountered:{}".format(self.tokens.command)
            )
        try:
            self.tokens.aid=HashString(self.tokens.args[0])
            self.tokens.apsk=HashString(self.tokens.args[1])
            self.tokens.server=FQDN(self.tokens.args[2])
        except Exception as e:
            print "Error:{}".format(e)
            sys.exit(1)    

    def dump(self):
        return "AID:'{}', APSK:'{}', SERVER:'{}'".format(
            self.tokens.aid(),
            self.tokens.apsk(),
            self.tokens.server()
        )

if __name__=="__main__":
    print "This is a class module and should not be run directly."