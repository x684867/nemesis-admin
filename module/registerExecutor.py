#!/usr/bin/env python
#
# registerExecutor.py 
# CLI agent registration utility.
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-8
#
import os
import sys
import logging
from tokenClasses import HashString, FQDN
from executorBase import Executor

class RegisterExecutor (Executor):
    def __init__(self,token):
        try:
            if token.command != "register":
                raise Exception ("Bad routing.  Expected register command.  Encountered:{}".format(self.tokens.command)
                )
            if not HashString(token.aid()).isHashString():
                raise Exception("Error: parser has not defined AID")
            if not HashString(token.apsk()).isHashString():
                raise Exception("Error: parser has not defined APSK")
            if not FQDN(token.server()).isFQDN():
                raise Exception("Error: parser has not defined Server FQDN")
        except Exception as e:
            print "RegisterExecutor Error:{}".format(e)

    def execute(self,token):
        try:
            self.runRemote(
                token.command,
                "{} {} {}".format(
                    token.aid(),
                    token.apsk(),
                    token.server()
                )
            )
        except Exception as e:
            logging.error("registerExecutor::execute():{}".format(e))
            sys.exit(1)
