#!/usr/bin/env python
#
# cliParser.py 
# Main Parser Class for Nemesis Admin CLI
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-18
#
import sys
from coreParser import CoreParser 
from validatorBase import Validator

class cliParser:
    """
        class Constructor:
            Requires a list of commands which are to be supported.
            These commands must have a validator class and module
            which will be loaded dynamically.
    """
    def __init__(self,commandlist=[]):

        core=CoreParser()
        token=core.tokens

        if token.command in commandlist:
            print "token.command is in commandList:{}".format(token.command)
            print "commandlist={}".format(commandlist)
            try:
                print "dynamic load of module/class starts"
                moduleName=token.command+"Parser"
                className=token.command.title()+"Parser"
                print "importing {}".format(moduleName)
                module=__import__(moduleName)
                try:
                    print "loading class:{}".format(className)
                    class_=getattr(module,className)
                    #instantiate the validator and pass our raw tokens in.
                    print "instantiating parser"
                    try:
                        parser=class_(token)
                    except Exception as e:
                        print "error creating validator({}):{}".format(className,e
                        )
                        sys.exit(1)
                    try:
                        self.tokens=parser.tokens
                    except Exception as e:
                        print "Error reading parser tokens:{}".format(e)
                except Exception as e:
                    print "Unhandled Error loading validator:{}".format(e)
                    sys.exit(1)
            except Exception as e:
                print "Error loading validator({}): {}".format(moduleName,e)
                sys.exit(1)
        else:
            print("Unrecognized or unknown command:{}".format(token.command))
            sys.exit(1)
        #Validator is loaded
        #If we are still running, we must assume the args were valid for the
        #specific command.  Let's execute something against the validator.

    @property 
    def tokens(self):
        return self.__tokens

    @tokens.setter
    def tokens(self,value):
        if isinstance(value,validator):
            self.__tokens=value
        else:
            print "cliParser.tokens setter expects Validator() child class."
            sys.exit(1)

"""



"""
if __name__=="__main__":
    print """
                This file defines the cliParser class for Nemesis 
                Command-Line Administrator Tool.

                Executing this script directly will only execute unit 
                tests.  It is recommended that you execute this file
                with the following syntax for testing:

                python cliParser.py command arg1 arg2 arg3 arg4 arg5
    
                Testing...
    """
    p=cliParser(['help'])
    print "cliParser Results:{}".format(p.tokens.dump())
