#!/usr/bin/env python
#
# helpParser.py 
# CLI help utility.
# (c) 2015 Sam Caldwell.  MIT License.
#
# See Ticket: https://samcaldwell.atlassian.net/browse/NADMIN-24
#
import sys
import logging
from validatorBase import Validator

class HelpParser (Validator):


    def __init__(self,tokens):
        logging.debug("HelpParser::__init__()")
        self.tokens=tokens
        if self.tokens.command!="help":
            raise Exception ("Bad routing.  Expected help command.  Encountered:{}".format(self.tokens.command)
            )
        if type(self.tokens.args[0]) != str:
            raise Exception("Error: expected string in args")
        self.tokens.context=self.tokens.args[0]


    @property
    def context(self):
        return self.tokens.__context


    @context.setter
    def context(self,value):
        if type(value) is str:
            self.tokens.__context=value
        else:
            raise Exception("Error: help context can only be a string.")
