#!/usr/bin/env python
#
# executorBase.py
# Base Class For the Validator Classes
# (c) 2015
#
#
import os
import sys
import fabric
from parsedTokens import ParsedTokens

class Executor:

    @property 
    def tokens(self):
        return self.__tokens

    @tokens.setter
    def tokens(self,value):
        if isinstance(value,ParsedTokens):
            self.__tokens=value
        else:
            raise Exception(
                "Error: Validators expect ParsedTokens instances only."
            )

    def __init__(self):
        pass

    def uploadFile(self,user,host,fileName):
        #
        # scp file and return objectId
        # objectId is the sha512 hash of a random string, 
        # the filename and timestamp.
        #
        pass

    def runRemote(self,user,commandString):
        swidth=int(os.popen('stty size', 'r').read().split()[1])
        try:
            print "="*swidth
            print "Executor::runRemote()  <not implemented>"
            print "="*swidth
            print "    user:         {}".format(user)
            print "    commandString:{}".format(commandString)
            print "="*swidth
        except Exception as e:
            raise("Error making remote SSH call: {}".format(e))
