#!/usr/bin/env python
#
#tokenClasses.py
#Generic token classes used by the Nemesis system.
#
# (c) 2015 Sam Caldwell.  MIT License.
#          mail@samcaldwell.net.
#
"""
"""
class HashString:
    __size=0
    __alg=''
    def __init__(self,hash_string,alg='sha512'):
        if alg=='sha512':
            self.__size=128
            self.__alg=alg
        else:
            raise Exception("Unrecognized hash algorithm")
        self.__call__(hash_string)
        
    def __call__(self,v=None):
        if v is None:
            return self.__value
        else:
            if self.isHashString(v):
                self.__value=v.strip()
            else:
                raise Exception("Type Mismatch.  Expected hash.")
            return v
    @property 
    def algorithm(self):
        return self.__alg

    def isHashString(self,s=None):
        h=['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
        try:
            if s is None:
                s=self()
            if type(s) is not str: return False
            s=s.strip()
            if len(s) == self.__size:
                return not (any((c not in h) for c in s.lower()))
            else:
                return False
        except Exception as e:
            raise Exception("isHashString encountered error:{}".format(e))
"""
"""   
class FQDN:
    __host=''
    def __init__(self,host):
        self.__host=self.__call__(host)

    def __call__(self,host=None):
        try:
            if host is None:
                return self.__host
            else:
                if self.isFQDN(host):
                    self.__host=host
                else:
                    raise Exception("Type mismatch.  Expect valid hostname")
                return host
        except Exception as e:
            raise Exception("tokenClasses::FQDN::__call__():{}".format(e))

    def isFQDN(self,s=None):
        try:
            if s is None:
                s=self.__host

            h=['-','_','.'] + \
                list(map(chr,range(ord('a'),ord('z')+1))) + \
                list(map(chr,range(ord('A'),ord('Z')+1))) + \
                range(0,9)
            s=s.strip()
            if type(s) is not str: return False
            if len(s) > 635: return False
            return not (any((c not in h) for c in s.lower()))
        except Exception as e:
            raise Exception("tokenClasses::FQDN::isFQDN():{}".format(e))

if __name__ == "__main__":
    print "This is a class module and shouldn't be used directly."
