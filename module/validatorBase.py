#!/usr/bin/env python
#
# validatorBase.py
# Base Class For the Validator Classes
# (c) 2015
#
#
from tokenClasses import HashString
from tokenClasses import FQDN

class Validator:

    @property 
    def tokens(self):
        return self.__tokens

    @tokens.setter
    def tokens(self,value):
        if isinstance(value,parsedTokens):
            self.__tokens=value
        else:
            raise Exception(
                "Error: Validators expect parsedTokens instances only."
            )

    def isString(self,s):
        return (type(s) is string)

    def isHashString(self,s):
        return HashString.isHashString(s)

    def isFQDN(self,s):
        return FQDN.isFQDN(s)

    def dump(self):
        return "tokens:{},source:'Validator'".format(self.tokens.dump())

    def __init__(self):
        pass
