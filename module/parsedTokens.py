#!/usr/bin/env python
#
# parsedTokens.py 
# Data Object used to store parsed tokens from CLI
# (c) 2015 Sam Caldwell.  MIT License.
#
#
class ParsedTokens:
    __command=""
    __args=[]

    def dump(self):
        return "command:'{}',args:{}".format(self.command,self.args)

    @property
    def command(self):
        return self.__command
        
    @command.setter
    def command(self,value):
        if type(value) is str:
            self.__command = value.strip()
        else:
            raise Exception("ParsedTokens expects string as command")
    
    @property
    def args(self):
        return self.__args
    
    @args.setter    
    def args(self, value):
        if type(value) is list:
            self.__args = value
        else:
            raise Exception("ParsedTokens expects list of args <array>")
    
    """
        Dump data into testable string.
    """
    
