#!/usr/bin/env python
#
# cliExecutor (class)
# Nemesis Admin Tool Cli Executor
# (c) 2015 Sam Caldwell.  MIT License
#
import os
import sys
import logging
from executorBase import Executor

class cliExecutor:
    __commandList=[]

    def __init__(self,commandList=[]):
        self.commands=commandList
        pass

    def execute(self,token):
        print "cliExecutor::execute():{}".format(token.command)
        if token.command in self.commands:
            try:
                moduleName=token.command+"Executor"
                className=token.command.title()+"Executor"
                module=__import__(moduleName)
                try:
                    print "loading class:{}.{}".format(moduleName,className)
                    class_=getattr(module,className)
                    print "class loaded.  instantiating..."
                    cmd=class_(token)
                    print "class instantiated"
                    if isinstance(cmd,Executor) is not True:
                        raise("Type mismatch. Executor ({}::{}) not child of executorBase.".format(moduleName,className)
                        )
                    try:
                        cmd.execute(token)
                    except Exception as e:
                        logging.error("Error in cmd.execute():{}".format(e))
                        sys.exit(4)
                except Exception as e:
                    logging.error("className: {} Error loading executor class: {}".format(className,e)
                    )
                    sys.exit(3)
            except Exception as e:
                print "Error loading executor Class({}): {}".format(moduleName,e)
                sys.exit(2)
        else:
            print("Unrecognized or unknown command:{}".format(token.command))
            sys.exit(1)

    @property 
    def commands(self):
        return self.__commandList

    @commands.setter
    def commands(self,value):
        if type(value) is list:
            self.commands=value
        else:
            logging.error("Type Mismatch.  Expected list/array in Executor")
            sys.exit(1) 

