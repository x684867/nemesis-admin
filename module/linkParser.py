#!/usr/bin/env python
#
# linkParser.py 
# Link Command Parser Class for Nemesis Admin CLI
# (c) 2015 Sam Caldwell.  MIT License.
#

import sys
from tokenClasses import HashString,FQDN
from validatorBase import Validator
"""

"""
class linkParser(Validator):
    """

    """
    def __init__(self,tokens):
        self.tokens=tokens
        if self.tokens.command!="link":
            raise Exception ("Bad routing. Expected link command.  Encountered:{}".format(self.tokens.command)
            )
        #
        # <fileName>
        #
        try:
            self.tokens.fileName=self.tokens.args[0]
        except Exception as e:
            print "Error:{}".format(e)
            sys.exit(1) 
        #
        # mode
        #
        try:
            if type(self.tokens.args[1]) is str:
                if self.tokens.args[1] in ['singleObject','keyValueObject']:
                    self.tokens.mode=self.tokens.args[1]
                else:
                    raise Exception("Unknown or unrecognized mode.  Expect ['singleObject','keyValueObject']")
            else:
                raise Exception("Type mismatch: mode should be a string.")
        except Exception as e:
            print "Error:{}".format(e)
            sys.exit(1) 
        #
        # --delimiter
        #
        try:
            if type(self.tokens.args[2]) is str:
                if self.tokens.args[2] == '--delimiter':
                    #
                    # <delimiter char>
                    #
                    if type(self.token.args[3]) is str:
                        if len(self.token.args[3]) >=1:
                            self.tokens.delimiter=self.tokens.args[3].strip()
                        else:
                            raise Exception("delim length must be >=1 char")
                    else:
                        raise Exception("Type mismatch: delim must be str")
                else:
                    raise Exception("Unknown argument ({})".format(
                        self.tokens.args[2])
                    )
            else:
                raise Exception("bad option.  expected --delimiter")
    """

    """
    def dump(self):
        return "fileName: {},{},{}".format(
            self.tokens.fileName(),
            self.tokens.mode(),
            self.tokens.delimiter()
        )


if __name__=="__main__":
    print "This is a class module and should not be run directly."